import {Component, ViewChild} from '@angular/core';
import {FeatureModalComponent} from "./feature-modal/feature-modal.component";
import {TagModalComponent} from "./tag-modal/tag-modal.component";
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild(MatSort) sort: MatSort;

  features: any = [];
  featuresPassed: number = 0;
  featuresFailed: number = 0;
  featuresSkipped: number = 0;
  featuresAmbiguous: number = 0;

  featuresState: any[] = [];

  scenarios: any[];
  scenariosPassed: number = 0;
  scenariosFailed: number = 0;
  scenariosSkipped: number = 0;
  scenariosPending: number = 0;
  scenariosAmbiguous: number = 0;
  scenariosNotDefined: number = 0;

  scenariosState: any[] = [];

  tags: Array<TagElement> = [];

  displayedColumns = ['name', 'tags', 'duration', 'total', 'passed', 'failed', 'pending', 'skip', 'undefined', 'ambiguous'];
  dataSource: MatTableDataSource<DataElement>;

  goldenData: Array<DataElement>;

  inError: boolean = false;

  constructor(public dialog: MatDialog) {
    this.loadJSON(response => {
      // Parse JSON string into object
      try {
        let result = (JSON.parse(response) as Array<any>); // .sort((a1, a2) => (a1.path).localeCompare(a2.path));
        this.calculate(result);
        this.features = result;
        this.inError = false;
      } catch (e) {
        console.log(e)
        this.inError = true;
      }
    });
  }

  calculate(features: Array<any>) {
    const arrays = features.map(it => it.elements);
    this.scenarios = [].concat.apply([], arrays);


    let dataSource: Array<DataElement> = [];
    features.forEach(feature => {
      feature.duration = 0;
      feature.scenarios = {};
      feature.scenarios.total = 0;
      feature.scenarios.passed = 0;
      feature.scenarios.failed = 0;
      feature.scenarios.pending = 0;
      feature.scenarios.skipped = 0;
      feature.scenarios.notdefined = 0;
      feature.scenarios.ambiguous = 0;

      this.parseScenarios(feature);

      let status = '';

      if (feature.isFailed) {
        this.featuresFailed++;
        feature.failed++;
        status = 'exclamation-circle failed-color';
      } else if (feature.isAmbiguous) {
        this.featuresAmbiguous++;
        feature.ambiguous++;
        status = 'flash ambiguous-color';
      } else if (feature.isSkipped) {
        feature.skipped++;
        this.featuresSkipped++;
        status = 'arrow-circle-right skipped-color';
      } else {
        feature.passed++;
        this.featuresPassed++;
        status = 'check-circle passed-color';
      }

      dataSource.push(<DataElement>{
        name: feature.name,
        tags: feature.tags,
        status: status,
        duration: this.msToTime(feature.duration),
        total: feature.scenarios.total,
        passed: feature.scenarios.passed,
        failed: feature.scenarios.failed,
        pending: feature.scenarios.pending,
        skip: feature.scenarios.skipped,
        undefined: feature.scenarios.notdefined,
        ambiguous: feature.scenarios.ambiguous,
        feature: feature
      });
    });

    this.tags = this.tags
    .filter(it => it.name.length < 30)
    .sort((o1, o2) => o2.scenarios.length - o1.scenarios.length);

    this.tags.forEach(tag => {
      const set = new Set(tag.scenarios);
      tag.scenarios = [];
      set.forEach(element => tag.scenarios.push(element));
      tag.scenarios.forEach(scenario => {
        if (scenario.failed > 0) {
          tag.failed++;
          return;
        }

        if (scenario.ambiguous > 0) {
          tag.ambiguous++;
          return;
        }

        if (scenario.notdefined > 0) {
          tag.undefined++;
          return ;
        }

        if (scenario.pending > 0) {
          tag.pending++;
          return;
        }

        if (scenario.skipped > 0) {
          tag.skip++;
          return;
        }

        if (scenario.passed > 0) {
          tag.passed++;
          return;
        }
      });

      let status = 'check-circle passed-color';

      if (tag.failed > 0) {
        status = 'exclamation-circle failed-color';
      } else if (tag.ambiguous > 0) {
        status = 'flash ambiguous-color';
      }

      tag.status = status;

    });

    this.goldenData = dataSource;
    this.dataSource = new MatTableDataSource(dataSource);
    this.dataSource.sort = this.sort;

    this.featuresState = [
      {
        "name": "Passed",
        "value": this.featuresPassed
      },
      {
        "name": "Failed",
        "value": this.featuresFailed
      },
      {
        "name": "Skipped",
        "value": this.featuresSkipped
      },
      {
        "name": "Ambiguous",
        "value": this.featuresAmbiguous
      }
    ];

    this.scenariosState = [
      {
        "name": "Passed",
        "value": this.scenariosPassed
      },
      {
        "name": "Failed",
        "value": this.scenariosFailed
      },
      {
        "name": "Pending",
        "value": this.scenariosPending
      },
      {
        "name": "Skipped",
        "value": this.scenariosSkipped
      },
      {
        "name": "Ambiguous",
        "value": this.scenariosAmbiguous
      },
      {
        "name": "Not defined",
        "value": this.scenariosNotDefined
      }
    ]
  }


  parseScenarios(feature) {
    feature.elements.forEach(scenario => {
      scenario.passed = 0;
      scenario.failed = 0;
      scenario.notdefined = 0;
      scenario.skipped = 0;
      scenario.pending = 0;
      scenario.ambiguous = 0;
      scenario.duration = 0;
      scenario.feature = feature;

      scenario = this.parseSteps(scenario);

      let allTags: any[] = [];

      if (feature.tags) {
        feature.tags.forEach(it => allTags.push(it));
      }

      if (scenario.tags) {
        scenario.tags.forEach(it => allTags.push(it));
      }

      if (scenario.duration > 0) {
        feature.duration += scenario.duration;
      }

      allTags.forEach(tag => {
        let result = this.tags.findIndex(it => it.name == tag.name);
        if (result != -1) {
          this.tags[result].scenarios.push(scenario);
        } else {
          let tagScenarios: any[] = [];
          tagScenarios.push(scenario);
          this.tags.push(<TagElement>{
            name: tag.name,
            scenarios: tagScenarios,
            status: status,
            failed: 0,
            ambiguous: 0,
            undefined: 0,
            passed: 0,
            pending: 0,
            skip: 0,
            feature: feature
          });
        }
      });

      if (scenario.failed > 0) {
        this.scenariosFailed++;
        feature.scenarios.total++;
        feature.isFailed = true;
        return feature.scenarios.failed++;
      }

      if (scenario.ambiguous > 0) {
        this.scenariosAmbiguous++;
        feature.scenarios.total++;
        feature.isAmbiguous = true;
        return feature.scenarios.ambiguous++;
      }

      if (scenario.notdefined > 0) {
        this.scenariosNotDefined++;
        feature.scenarios.total++;
        return feature.scenarios.notdefined++;
      }

      if (scenario.pending > 0) {
        this.scenariosPending++;
        feature.scenarios.total++;
        return feature.scenarios.pending++;
      }

      if (scenario.skipped > 0) {
        this.scenariosSkipped++;
        feature.scenarios.total++;
        return feature.scenarios.skipped++;
      }

      if (scenario.passed > 0) {
        this.scenariosPassed++;
        feature.scenarios.total++;
        return feature.scenarios.passed++;
      }
    });
  }

  parseSteps(scenario) {
    if(!scenario.steps) return scenario;
    scenario.steps.forEach(step => {
      if (step.result.duration) {
        scenario.duration += step.result.duration;
      }

      if (step.result.status === 'passed') {
        return scenario.passed++;
      }

      if (step.result.status === 'failed') {
        return scenario.failed++;
      }

      if (step.result.status === 'undefined') {
        return scenario.notdefined++;
      }

      if (step.result.status === 'pending') {
        return scenario.pending++;
      }

      if (step.result.status === 'ambiguous') {
        return scenario.ambiguous++;
      }
      scenario.skipped++;
    });

    return scenario;
  }

  msToTime(duration: number): string {
    return (Math.round(duration / 10000000 / 60) / 100) + ' min';
  }

  openModal(feature: any) {
    this.dialog.open(FeatureModalComponent, {
      width: '100%',
      height: '100%',
      data: feature
    });

    return false;
  }

  loadJSON(callback) {

    let xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', 'assets/cucumber.json', true);
    xobj.onreadystatechange = () => {
      if (xobj.readyState == 4 && xobj.status == 200) {
        callback(xobj.responseText);
      }
    };
    xobj.send(null);
  }

  showOnly(type: string) {
    if (type == 'failed') {
      this.dataSource = new MatTableDataSource<DataElement>(this.goldenData.filter(it => it.feature.isFailed));
    }
    if (type == 'passed') {
      this.dataSource = new MatTableDataSource<DataElement>(this.goldenData.filter(it => !it.feature.isFailed && !it.feature.isAmbiguous));
    }
    if (type == 'ambiguous') {
      this.dataSource = new MatTableDataSource<DataElement>(this.goldenData.filter(it => it.feature.isAmbiguous));
    }
  }

  showTag(tag: TagElement) {
    this.dialog.open(TagModalComponent, {
      width: '100%',
      height: '100%',
      data: tag
    });
  }

  reset() {
    this.dataSource = new MatTableDataSource<DataElement>(this.goldenData)
  }
}

export interface DataElement {
  name: string;
  tags: string;
  status: string;
  duration: string;
  total: number;
  passed: number;
  failed: number;
  pending: number;
  skip: number;
  undefined: number;
  ambiguous: number;
  feature: any;
}

export interface TagElement {
  name: string;
  scenarios: any[];
  status: string;
  passed: number;
  failed: number;
  pending: number;
  skip: number;
  undefined: number;
  ambiguous: number;
  feature: any;
}
