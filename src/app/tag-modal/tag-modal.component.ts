import {Component, Inject, OnInit} from '@angular/core';
import {TagElement} from '../app.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  templateUrl: './tag-modal.component.html',
  styleUrls: ['./tag-modal.component.scss']
})
export class TagModalComponent implements OnInit {
  scenariosState: any[];
  goldenData: any[];
  elements: any[];

  constructor(
    public dialogRef: MatDialogRef<TagModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TagElement
  ) {
    this.goldenData = data.scenarios;
    this.elements = data.scenarios;

    this.scenariosState = [
      {
        "name": "Passed",
        "value": data.passed
      },
      {
        "name": "Failed",
        "value": data.failed
      },
      {
        "name": "Pending",
        "value": data.pending
      },
      {
        "name": "Skipped",
        "value": data.skip
      },
      {
        "name": "Ambiguous",
        "value": data.ambiguous
      },
      {
        "name": "Not defined",
        "value": data.undefined
      }
    ]
  }

  ngOnInit() {
  }

  getDauer(dauer: number): string {
    return (Math.round(dauer / 1000000) / 1000) + 's'
  }

  getIcon(scenario: any): string {
    if (scenario.failed) {
      return 'exclamation-circle failed-color';
    } else if (scenario.ambiguous) {
      return 'flash ambiguous-color';
    } else if (scenario.skipped) {
      return 'arrow-circle-right skipped-color';
    }else if (scenario.notdefined) {
      return 'question-circle not-defined-color';
    } else if (scenario.pending) {
      return 'minus-circle pending-color';
    } else {
      return 'check-circle passed-color';
    }
  }

  getIconForStep(step: any): string {
    if (step.result.status === 'passed') {
      return 'check-circle passed-color';
    }

    if (step.result.status === 'failed') {
      return 'exclamation-circle failed-color';
    }

    if (step.result.status === 'undefined') {
      return 'question-circle not-defined-color';
    }

    if (step.result.status === 'pending') {
      return 'minus-circle pending-color';
    }

    if (step.result.status === 'ambiguous') {
      return 'flash ambiguous-color';
    }
    return 'arrow-circle-right skipped-color';
  }

  showOnly(type: string) {
    if(type == 'failed') {
      this.elements = this.goldenData.filter(it => it.failed);
    }
  }

  reset() {
    this.elements = this.goldenData;
  }
}
